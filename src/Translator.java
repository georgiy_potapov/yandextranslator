import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Translator {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader( new InputStreamReader( System.in ) );
		
		while(true)
		{	
			System.out.println("enter text:");
			try
			{
				String toTranslate = reader.readLine().replaceAll(" ", "+");
				
				StringBuilder request = new StringBuilder();
			
				request.append("https://translate.yandex.net/api/v1.5/tr/translate")
						.append('?')
						.append("key=trnsl.1.1.20181010T090540Z.938a057cc9fdad1b.5c6e69a16de9542a461be3c7b122b06e9f53e7db")
						.append('&')
						.append("text=").append(toTranslate)
						.append('&')
						.append("lang=en-ru");

				URL urlText = new URL( request.toString() );
				
				//int responseCode = connect.getResponseCode();
				String response = null;  // ����� �� ���				
				HttpURLConnection connect = (HttpURLConnection) urlText.openConnection();					
				connect.setRequestMethod("GET");
				
				try( BufferedReader responser = new BufferedReader( new InputStreamReader( connect.getInputStream(), "UTF-8" ) ) )
				{				
					StringBuffer tmpResponse = new StringBuffer();
					String inputLine = null;
					while ((inputLine = responser.readLine()) != null) 
					{
						tmpResponse.append(inputLine);
					}
					response = tmpResponse.toString();
				}
				finally {
					connect.disconnect();
				}
							
				//������ ����� ��� ��������� ������������� ������
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

				DocumentBuilder builder = factory.newDocumentBuilder();
				
				Document doc = builder.parse( new InputSource(new StringReader(response) ) );
				
				doc.getDocumentElement().normalize();
				
				int respponseApiCode = Integer.parseInt( doc.getDocumentElement().getAttribute("code") );
				
				if( respponseApiCode == 200 )
				{	
					NodeList textTranslation = doc.getElementsByTagName("text");
					String text = textTranslation.item(0).getTextContent();
					text = text.replaceAll("\\+", " ");
					
					System.out.println( "translated text:" );
					System.out.println( text );
				}
				else
					System.out.println( "translation failed with error code " + respponseApiCode );
			} 
			catch (MalformedURLException e) {
				System.out.println( e.getMessage() );
				e.printStackTrace();
				continue;
			}
			catch ( IOException | ParserConfigurationException | SAXException e) 
			{
				System.out.println(e.getMessage() );
				e.printStackTrace();
				continue;
			}

		}			   
	}

}
